#External Library
from flask import Flask, request as flask_request
import json
from werkzeug.exceptions import HTTPException

import logging
import log
# Internel Library
import YouTubeSpeechRecognition

app = Flask(__name__)
RESTful_version = "v1"

@app.errorhandler(HTTPException)
def handle_http_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "error": {
            "code": e.code,
            "error_name": e.name,
            "message": e.description
        }
    })
    response.content_type = "application/json"
    return response

def handle_exception(e, code):
    """Return JSON instead of HTML for HTTP errors."""
    response = json.dumps({
        "error": {
            "code": code,
            "message": str(e)
        }
    })
    return response, code, {'Content-Type': 'application/json'}

@app.route("/{}/healthcheck".format(RESTful_version), methods=["GET"])
def healthcheck():
    log.error("Healthcheck")
    return "Welcome to speech-recognition-api  via flask framework.", 200, {'Content-Type': 'text'}

@app.route("/{}/youtube/speech".format(RESTful_version), methods=['GET'])
def get_speech():
    try:
        required_parameter = ['url']
        output = {}
        for param in required_parameter:
            if param not in flask_request.form:
                raise Exception("Request parameter '{}' not found".format(param))

        url = flask_request.form['url']
        log.info("[DEBUG] Input : {}".format(url))

        output["text"]  = YouTubeSpeechRecognition.get_text_from_video(url)

        log.info("[DEBUG] Output : {}".format(output))
        
        return  output,200,{'Content-Type': 'application/json'}
    except Exception as e:
        message = 'Error : {}'.format(str(e))
        log.error(message)
        return handle_exception(e=message,code=400)

if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    log.set_level_log(gunicorn_logger.level)
    log.error("speech-recognition-api ready!!!")