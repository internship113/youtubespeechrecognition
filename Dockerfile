# Stage 1 - Install build dependencies
FROM python:3.8-slim-buster AS builder
ARG PROJECT_NAME=youtube-speech-recognition

RUN apt-get update && \
    apt-get install -y --no-install-recommends build-essential gcc && \
    apt-get install -y git && \
    apt-get install -y git bzip2 nodejs libssl-dev protobuf-compiler libsndfile1-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/*
    

WORKDIR /app
RUN python -m venv .venv && .venv/bin/pip install --no-cache-dir -U pip setuptools
COPY requirements.txt .
RUN .venv/bin/pip install --no-cache-dir -r requirements.txt && find /app/.venv
# RUN pip3 install  --no-cache-dir -r requirements.txt


# Stage 2 - Copy only necessary files to the runner stage
FROM python:3.8-slim-buster
ENV PATH="/app/.venv/bin:$PATH"

WORKDIR /app
RUN apt-get -y update
RUN apt-get install -y ffmpeg
RUN apt-get clean && rm -rf /var/lib/apt/lists/*
COPY --from=builder /app /app
COPY . .


# Set env
ENV PROJECT_NAME ${PROJECT_NAME}
ENV APP_ENV beta
ENV PORT 8080
ENV LOG_LEVEL error
ENV TZ UTC

# The EXPOSE instruction indicates the ports on which a container 
# will listen for connections
EXPOSE ${PORT}

# Cammand
# CMD python3 main.py -env $APP_ENV --port $PORT --log-level $LOG_LEVEL
CMD gunicorn route:app --threads=10 --workers=1 --bind=0.0.0.0:${PORT} --timeout=1800 --log-level=${LOG_LEVEL}