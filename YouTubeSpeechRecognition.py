import os
import torch
import youtube_dl
from tqdm import tqdm
from pyannote.audio import Model
from pyannote.audio.pipelines import VoiceActivityDetection
from transformers import pipeline
from collections import Counter
import cld3

import log

#pipeline for thai speech to text language
PIPELINE_TH = pipeline(model='wannaphong/wav2vec2-large-xlsr-53-th-cv8-newmm',
                       tokenizer='wannaphong/wav2vec2-large-xlsr-53-th-cv8-newmm',
                       task='automatic-speech-recognition')

#pipeline for eng speech to text language
PIPELINE_EN = pipeline(model='jonatasgrosman/wav2vec2-large-xlsr-53-english',
                       tokenizer='jonatasgrosman/wav2vec2-large-xlsr-53-english',
                       task='automatic-speech-recognition')


#Pyannote config
PYANNOTEMODEL = Model.from_pretrained("MODEL/pyannote_model.bin")
HYPER_PARAMETERS = {
  # onset/offset activation thresholds
  "onset": 0.5, "offset": 0.5,
  # remove speech regions shorter than that many seconds.
  "min_duration_on": 0.0,
  # fill non-speech regions shorter than that many seconds.
  "min_duration_off": 0.0
}
PYANNOTEPIPELINE = VoiceActivityDetection(segmentation=PYANNOTEMODEL)
PYANNOTEPIPELINE.instantiate(HYPER_PARAMETERS)

#youtube_dl config
YDL_OPTS = {
            'outtmpl': 'file.%(ext)s',
            'format': 'bestaudio',
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'wav',
            }]}


#language support
LANG_SUPPORT = ['th', 'en']

#auto select device: CPU or GPU
DEVICE_NUM = [0 if torch.cuda.is_available() else -1][0]


#Combine pipeline component for each a language
def pipeline_process(processor, model, config):
    decoder = processor.decoder
    feature_extractor = processor.feature_extractor
    tokenizer = processor.tokenizer
    return pipeline("automatic-speech-recognition", config=config, model=model, 
                tokenizer=tokenizer,feature_extractor=feature_extractor, 
                decoder=decoder)

def YouTubeLanguageDetection(infomation):
    def TagsDetect(infomation):
        try:
            tags = []
            for i in infomation['tags']:
                tags.append(cld3.get_frequent_languages(i, num_langs=3))
            tags_lang = [
                item.language for sublist in tags for item in sublist]
            tags_Prob = [
                item.probability for sublist in tags for item in sublist]
            return dict(zip(tags_lang, tags_Prob))
        except:
            return dict()
    def TitleDetect(infomation):
        try:
            title = cld3.get_frequent_languages(
                infomation['title'], num_langs=3)
            title_lang = [sublist.language for sublist in title]
            title_Prob = [sublist.probability for sublist in title]
            return dict(zip(title_lang, title_Prob))
        except:
            return dict()
    def DescriptionDetect(infomation):
        try:
            description = cld3.get_frequent_languages(
                infomation['description'], num_langs=3)
            description_lang = [
                sublist.language for sublist in description]
            description_Prob = [
                sublist.probability for sublist in description]
            return dict(zip(description_lang, description_Prob))
        except:
            return dict()
    title_lang = Counter(TitleDetect(infomation))
    description_lang = Counter(DescriptionDetect(infomation))
    tag_lang = Counter(TagsDetect(infomation))
    languageYouTube = dict(title_lang+description_lang+tag_lang)
    result = {key: value / 3 for key, value in languageYouTube.items()}
    return max(result, key=result.get)

def __download_audio__(link, info):
    try:
        ydl = youtube_dl.YoutubeDL(YDL_OPTS)
        ydl.download([link])
        info_with_audio_extension = dict(info)
        info_with_audio_extension['ext'] = 'wav'
        return ydl.prepare_filename(info_with_audio_extension)
    except Exception as e:
        os.remove('file.wav')
        print(e)

def __tqdm_enumerate__(iter):
    i = 0
    for y in tqdm(iter):
        yield i, y
        i += 1

def __ASR__(link, info, pipeline, STT):
    try:
        log.error("download audio")
        file = __download_audio__(link=link, info=info)
        log.error("PYANNOTEPIPELINE")
        output = PYANNOTEPIPELINE(file)
        log.error("FOR LOOP tqdm_enumerate")
        for i, speech in __tqdm_enumerate__(output.get_timeline().support()):
            text = str(speech).replace(']', '')
            text = text.replace('[', '')
            tm = text.strip().split(' -->  ')
            log.error("ffmpeg")
            os.system(
                f'ffmpeg  -i {file} -ss {tm[0]} -to {tm[1]}  file_{i}.wav -loglevel quiet')
            pipe = pipeline
            log.error("pipe")
            text = pipe(f'file_{i}.wav', chunk_length_s=10,
                        stride_length_s=(4, 2), device=DEVICE_NUM)
            STT += text['text']
            log.error("error remvoe file_i.wav")
            os.remove(f'file_{i}.wav')
        log.error("end for loop")
        os.remove('file.wav')
    except Exception as e:
        log.error(e)
        os.remove('file.wav')
        print(e)
    log.error(STT)
    return STT


#main speech to text fuction
def get_text_from_video(link):
    STT = ''
    info = youtube_dl.YoutubeDL(YDL_OPTS).extract_info(link, download=False)
    lang = YouTubeLanguageDetection(info)
    assert lang in LANG_SUPPORT, f'this tools support only {",".join(LANG_SUPPORT)} language'
    if lang == 'th':
        pipeline = PIPELINE_TH
    elif lang == 'en':
        pipeline = PIPELINE_EN
    else: raise Exception("Import Pipeline Error")
    text = __ASR__(link=link, info=info, pipeline=pipeline, STT=STT)
    return text


