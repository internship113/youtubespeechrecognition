# speech-recognition-api

Project initial https://veteran.socialenable.co/se4/socialenable/issues/4249

ตัวโปรแกรมเป็นการเรียกใช้ Flask framework ซึ่งเป็น Web framework เข้ามาช่วยในการเปิด route ต่างๆภายในโปรแกรมและใช้คำสั่ง **gunicorn** ในการกำหนด threads, worker, IP, Port, Log level และ timeout 

โดย file หลักที่เรียกใช้คือ **route.py** และมี Internal library 2 ไฟล์คือ **log.py** และ **YoutubeSpeechRecognition.py**

สิ่งที่ต้องทำก่อนรันโปรแกรม

1.ลง library ทั้งหมดตามไฟล์ **requirements.txt** ครบทุกตัวโดยใช้คำสั่ง `pip install -r requirements.txt`

2.เวอร์ชั่นของ python ต้อง 3.9 หรือต่ำกว่า

3.ลง **ffmpeg**

ตัวอย่างคำสั่งในการรัน: 

`gunicorn route:app --threads=10 --workers=1 --bind=0.0.0.0:${PORT} --timeout=1800 --log-level=${LOG_LEVEL}`

ตอนโปรแกรมเรื่มทำงานครั้งแรกจะทำการโหลดไฟล์ model จาก "https://huggingface.co/" ให้เสร็จก่อนโปรแกรมถึงจะพร้อมใช้งานโดยระยะเวลาการรอขึ้นอยู่กับความเร็วอินเตอร์เน็ตในขณะนั้น

**มีทั้งหมด 2 path**

path แรกเป็น `/healthcheck`จะเป็นการเช็คความพร้อมของ API โดยจะคืนค่ากลับไปเป็น "Welcome to speech-recognition-api  via flask framework." 

path ที่สองเป็น `/youtube/speech` โดย path นี้เป็นการรับค่าเป็นที่อย่ของวีดีโอหรือ URL รองรับเฉพาะวีดีโอบน Youtube เท่านั้น
ตัวแปรที่รับเ้ขาเป็น from-data ซึ่งมีรูปแบบดังนี้ Key = `url` Value = `URL ของวีดีโอ`
ในการทำ speech recognition นั้นมีการเรียกใช้ Function ในไฟล์ **YoutubeSpeechRecognition.py** ชื่อ **get_text_from_video(url) **โดยเรียกใช้จากไฟล์ **route.py**  และส่งค่ากลับไปเป็นรูปแบบ `JSON` โดยมีโครงสร้างเป็น

{
"text" : "speech"
}
